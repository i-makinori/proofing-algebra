
# メモ

証明を探索するプログラム

探索結果を用いて帰納的学習を行うプログラム。証明可能な帰納的学習。

帰納的学習結果を探索方法に加えてゆく(...ルール空間(法則の空間)へのフィードバック...)システム

函数を定義しないプログラム開発手法

相互学習システム (人が学ぶとき、コンピュータも同時に学ぶ)、相互成長の機会をソフトウェアから提供する。

定理を学習して、学習した定理を用いて、新しい定理を説明するという、再帰的な学習進行が、このソフトウェアのコンセプトとなる。

逆函数と、超自然性。

逆関数を考慮しない超自然性。

# 当ソフトウェアでの、学習の位置付け

当ソフトウェアに於いて、学習を以下のように定義する。

