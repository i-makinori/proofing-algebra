
(in-package :proof-algebra)

;;;; notation

;;; utils



;;; order of operators

#|
(defparameter *priority-of-operators*
  '((1 "(~a~b)")
    (1 ("~a" "~b") "~a->~b" `(-> ,"~a" ,"~b")) ;;
    (2  = )
    ))
|#

(defparameter *priority-of-operators*
  '(;; (())
    ;; priority 0
    (->)
    ;; (_any _some) ;; one args form.
    (=)
    (^ _root) ;; x^a, x_root(y) ,
    (* /)
    (+ -)
    ;; priority N
    ))

(defparameter *infix-operators* '(= -> ^ _root * / + -))

;;; prefix -> infix

(defun prefix->infix (prefix &optional (priority *priority-of-operators*))
  (cond ((atom prefix)
         prefix)
        ((member (car prefix) *infix-operators*)
         (let ((func (prefix->infix (car prefix) priority))
               (terms (mapcar #'(lambda (l) (prefix->infix l priority)) (cdr prefix))))
           (reduce #'(lambda (s l) (append (list s func) l))
                   (reverse (cdr (reverse terms)))
                   :from-end t :initial-value (list (car (reverse terms))))))
        (t
         (let ((func (prefix->infix (car prefix) priority))
               (terms (mapcar #'(lambda (l) (prefix->infix l priority)) (cdr prefix))))
           (append (list func) terms)))))

;;; infix -> prefix

;;(defun flatten (nest-list)
;;  (apply #'append nest-list))

(defun the-priority-of (name priority)
  (labels ((iter (i l)
             (cond ((null l) nil)
                   ((member name (car l)) i)
                   (t (iter (+ i 1) (cdr l))))))
    (iter 0 priority)))

(defun function? (name priority)
  (the-priority-of name priority))

(defun number? (name priority)
  (and (not (consp name))
       (not (function? name priority))))

(defun infix->prefix-aux (infix priority opr opd)
  (cond
    ;; null infix
    ((and (null infix) (null opr))
     (car opd))
    ((null infix)
     (infix->prefix-aux infix priority (cdr opr)
                        (cons (list (car opr) (cadr opd) (car opd))
                              (cddr opd))))
    ;; number
    ((number? infix priority)
     (infix->prefix-aux '() priority opr (cons infix opd)))
    ;; head is number
    ((number? (car infix) priority)
     (infix->prefix-aux (cdr infix) priority opr (cons (car infix) opd)))
    ;; inner parenth
    ((consp (car infix))
     (infix->prefix-aux (cdr infix) priority opr
                        (cons (infix->prefix (car infix) priority) opd)))
    ;; calculational operator
    ((null opr)
     (infix->prefix-aux (cdr infix) priority (cons (car infix) opr) opd))
    ((< (the-priority-of (car infix) priority)
        (the-priority-of (car opr) priority))
     (infix->prefix-aux (cdr infix) priority (cons (car infix) opr) opd))
    (t
     (infix->prefix-aux (cdr infix) priority (cons (car infix) (cdr opr))
                        (cons (list (car opr) (cadr opd) (car opd))
                              (cddr opd))))))

(defun infix->prefix (infix &optional (priority *priority-of-operators*))
  (infix->prefix-aux infix priority '() '()))



         
;;; tests
#|
> (prefix->infix '(* (+ 1 2) (+ 3 4 5)))
((1 + 2) * (3 + 4 + 5))
> (infix->prefix '((1 + 2) * (3 + 4 + 5)))
(* (+ 1 2) (+ (+ 3 4) 5))
> ;;
> (infix->prefix '(1 * 2 + 3 * (4 / 2 + 3)))
(+ (* 1 2) (* 3 (+ (/ 4 2) 3)))
> (prefix->infix (infix->prefix '(1 * 2 + 3 * (4 / 2 + 3))))
((1 * 2) + (3 * ((4 / 2) + 3)))
|#
