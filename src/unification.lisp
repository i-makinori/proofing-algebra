(in-package :proof-algebra)


;; Unification merges ... somwhat forgot it is somewhat-forgot.
;; ... Um? what is somewhat-forgot? ...
;; Oh.. I remember. somewhat-forgot is somewhat well known which is appered in another place,
;; formed as the pair of temporary key and its content.

;; (unification '(something) (Taro like the-Mu) (Taro like something) '())
;; (unification '(x) '(a x 2) '(a 4 2) '()) ;; --> ((x . 4)) also (x)

;;;; utils

(defun set-equalp (set1 set2)
  (cond ((equal set1 set2)
         t)
        ((or (atom set1) (atom set2))
         nil)
        ((member (car set1) set2 :test #'equal)
         (set-equalp (remove (car set1) set1 :test #'equal)
                     (remove (car set1) set2 :test #'equal)))
        (t nil)))

(defun lwarn (control-string &rest format-arguments)
  (let ((message (concatenate 'string "!!Warn!!: " control-string "~%")))
    (apply #'format (append (list t message) format-arguments))))

;;;; template unification engine

(defconstant +failure+ '+failure+)
(defconstant +any+ '+any+)


;;; basic function

(defun unknownp (symbol unknowns)
  "is unknown symbol at pattern?"
  (find symbol unknowns :test 'equal))

(defun decidedp (symbol unknowns)
  "is constatly symbol at pattern?"
  (not (unknownp symbol unknowns)))


(defun recursive-bind-search (unknown binds &optional (path (list unknown)))
  (let ((current-binded (cdr (car (remove-if-not #'(lambda (uu) (eql unknown (car uu))) binds)))))
    ;; (format t "~A, ~A~%" current-binded path)
    (cond ((null current-binded)
           (reverse path))
          ((member current-binded path :test #'equal)
           (reverse (cons current-binded path)))
          (t
           (recursive-bind-search current-binded binds (cons current-binded path))))))


;;; matching

#|
(defun maybe-register-to-bind-ignore-name-confrict (name value unknowns binds)
  ;;(format t "R: ~A, ~A, ~A, ~A~%" name value unknowns binds)
  (let* ((LoL-s ;; it wil generate same struct of "Let over Lambda"-Series.
           (recursive-bind-search name binds))
         (car-bindto (cadr lol-s)))
    ;; (format t "R: ~A ~A~%" name (unknownp name binds))
    (cond ((not (unknownp name unknowns))
           (lwarn "~A is not unknown struct" name)
           +failure+)
          ;;((equal name value)
          ;;binds)
          ((null car-bindto)
           (cons (cons name value) binds))
          ((equal value car-bindto)
           binds)
          ((and (not (equal value car-bindto))
                (unknownp car-bindto unknowns))
           (cons (cons name value) binds))
          ((and (not (equal value car-bindto))
                (not (unknownp car-bindto unknowns)))
           ;; == t.
           ;; decided struct appeared multiply different.
+failure+))))
|#

(defun maybe-register-to-bind-ignore-name-confrict (name value unknowns binds)
  (let* ((bind-same-name (find-if #'(lambda (nv) (equal (car nv) name)) binds)))
    (cond ((not (unknownp name unknowns))
           (lwarn "~A is not unknown struct" name)
           +failure+)
          ((null  bind-same-name)
           (cons (cons name value) binds))
          ((equal (cdr bind-same-name) value)
           binds)
          (t +failure+))))
        
  

(defun match-of-list^a1 (unknowns pattern-left pattern-right binds)
  (cond ((unknownp pattern-left unknowns)
         (maybe-register-to-bind-ignore-name-confrict pattern-left pattern-right unknowns binds))
        ((eql pattern-left pattern-right)
         binds)
        (t +failure+)))

(defun match-of-list^an (unknowns pattern-left pattern-right binds)
    (cond
      ((unknownp (car pattern-left) unknowns)
       (match-left unknowns (cdr pattern-left) (cdr pattern-right)
                   ;;(maybe-register-to-bind (car pattern-left) (car pattern-right) unknowns binds)))
                   (maybe-register-to-bind-ignore-name-confrict
                    (car pattern-left) (car pattern-right) unknowns binds)))
      (t
       (match-left unknowns (cdr pattern-left) (cdr pattern-right)
                   (match-left unknowns (car pattern-left) (car pattern-right) binds)))))

(defun match-left (unknowns pattern-left pattern-right binds)
  ;;(format t "M: ~A, ~A~%" pattern-left pattern-right)
  (cond
    ((eq binds +failure+) +failure+)
    ((and (null pattern-left) (null pattern-right))
     (values binds unknowns))
    ;;
    ((atom pattern-left)
     (match-of-list^a1 unknowns pattern-left pattern-right binds))
    ((and (listp pattern-left) (listp pattern-right))
     (match-of-list^an unknowns pattern-left pattern-right binds))
    (t +failure+)))


;;; unification


(defun maybe-register-to-bind (name value unknowns binds)
  (let* ((LoL-s ;; it wil generate same struct of "Let over Lambda"-Series.
           (recursive-bind-search name binds))
         (car-bindto (cadr lol-s)))
    ;; (format t "R: ~A ~A~%" name (unknownp name binds))
    (cond ((not (unknownp name unknowns))
           (lwarn "~A is not unknown struct" name)
           +failure+)
          ((equal name value)
           binds)
          ((null car-bindto)
           (cons (cons name value) binds))
          ((equal value car-bindto)
           binds)
          ((and (not (equal value car-bindto))
                (unknownp car-bindto unknowns))
           (cons (cons name value) binds))
          ((and (not (equal value car-bindto))
                (not (unknownp car-bindto unknowns)))
           ;; == t.
           ;; decided struct appeared multiply different.
           +failure+))))


(defun unification-of-list^a1 (unknowns pattern1 pattern2 binds)
  (let* ((binds-p1
           (if (unknownp pattern1 unknowns)
               (maybe-register-to-bind (car pattern1) (car pattern2) unknowns binds)
               binds))
         (binds-p2
           (if (and (unknownp pattern2 unknowns) (not (eq binds-p1 +failure+)))
               (maybe-register-to-bind (car pattern2) (car pattern1) unknowns binds-p1)
               binds-p1)))
  (cond
    ((or (unknownp pattern1 unknowns) (unknownp pattern2 unknowns))
     binds-p2)
    ((eql pattern1 pattern2) ;; pattern1 and pattern2 are decided
     binds)
    (t +failure+))))

(defun unification-of-list^an (unknowns pattern1 pattern2 binds)
  (let* ((binds-p1
           (if (unknownp (car pattern1) unknowns)
               (maybe-register-to-bind (car pattern1) (car pattern2) unknowns binds)
               binds))
         (binds-p2
           (if (unknownp (car pattern2) unknowns)
               (maybe-register-to-bind (car pattern2) (car pattern1) unknowns binds-p1)
               binds-p1)))
    (cond
      ((or (unknownp (car pattern1) unknowns) (unknownp (car pattern2) unknowns))
       (unification unknowns (cdr pattern1) (cdr pattern2) binds-p2))
      (t
       (unification unknowns (cdr pattern1) (cdr pattern2)
      (unification unknowns (car pattern1) (car pattern2) binds))))))


  
(defun unification (unknowns pattern1 pattern2 binds)
  "returns1: (unknown . its-value) list
returns2: unknowns"
  
  ;; note: when both of pattern1 and pattern2 are unknown,
  ;; this implement returns the set which has all binded unknowns at each pair's head of it's set.
  ;; it means that returns are larger sized binds list than minimals.
  
  ;;(format t "M: ~A . ~A . ~A~%" pattern1 pattern2 binds)
  (cond
    ((eq binds +failure+) +failure+)
    ((and (null pattern1) (null pattern2))
     (values binds unknowns))
    ;;
    ((or (atom pattern1) (atom pattern2))
     (unification-of-list^a1 unknowns pattern1 pattern2 binds))
    ((and (listp pattern1) (listp pattern2))
     (unification-of-list^an unknowns pattern1 pattern2 binds))
    (t +failure+)))

