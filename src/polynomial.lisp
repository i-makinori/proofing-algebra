(in-package :proof-algebra)
  




;;;; theorems

;;(defmacro theorem (name anys from into proof)
;; )

(defun theorem-s (name anys from into proof)
  (list name anys from into proof))

(defun theorem-name (theorem)
  (car theorem))
(defun theorem-anys (theorem)
  (cadr theorem))
(defun theorem-from (theorem)
  (caddr theorem))
(defun theorem-into (theorem)
  (cadddr theorem))
(defun theorem-proof (theorem)
  (cadddr theorem))

;;;; define first axioms

(defparameter *first-axioms*
  ;; (name unknowns form-before form-after . proof)
  (list
   ;; ∀(a, b) |- a = b => b = a.
   '(eq_ab=>e_qba (a b) (= a b) (= b a) ())
   ;;
   ;; n - n = 0
   '(n-n=0 (n) (- n n) 0 ())
   ;; a + b = b + a
   '(a+b=b+a (a b) (+ a b) (+ b a))
   ;; a * b = b * a
   '(a*b=b*a (a b) (* a b) (* b a))
   ;; a + (b + c) = (a + b) + c
   '(a+{b+c}={a+b}+c (a b c) (+ a (+ b c)) (+ (+ a b) c))
   ;; a * (b * c) = (a * b) * c
   '(a*{b*c}={a*b}*c (a b c) (* a (* b c)) (* (* a b) c))
   ;; c * (a + b) <=> c * a + c * b
   '(c*{a+b}={c*a}+{c*b} (c a b) (* c (+ a b)) (+ (* c a) (* c b)))
   '({c*a}+{c*b}=c*{a+b} (c a b) (+ (* c a) (* c b)) (* c (+ a b)))
   ))

(defparameter *problem1*
  '((i j k l)
    (+ (* i k) (* i l) (* j i) (* j l))
    (* (+ i j) (+ k l))))


;;;; apply theorem


#|
(defun maybe-apply-theorem (unknowns theorem formula)
  (let* ((binds-into
           (match-left (theorem-anys theorem) (theorem-from theorem) formula '()))
         (binds-check
           (every #'(lambda (bind) (unknownp (cdr bind) unknowns)) binds-into )))
    (cond ((eq binds-into +failure+)
           +failure+)
          ((not binds-check)
           +failure+)
          (t
           (apply-binds (theorem-anys theorem) (theorem-into theorem) binds-into)))))
|#



(defun maybe-apply-theorem (theorem formula)
  ;; not unknown sysmbols are also applied
  (let* ((binds-from '())
         (binds-into
           (match-left (theorem-anys theorem) (theorem-from theorem) formula binds-from)))
    (if (not (eq binds-into +failure+))
        (apply-binds (theorem-anys theorem) (theorem-into theorem) binds-into)
        +failure+)))

(defun maybe-apply-theorem-at (theorem formula at &key (message? nil))
  ;; not unknown sysmbols are also applied
  (let* ((after-apply (maybe-apply-theorem theorem (cnadr at formula))))
    ;;(format t "~A, ~A~%" (cnadr at formula) after-apply)
    (cond ((eql after-apply +failure+)
           (if message?
               (format t "apply ~A ~A into ~A for ~A of ~A is failed.~%"
                       (theorem-anys theorem) (theorem-from theorem) (theorem-into theorem)
                       at formula))
           +failure+)
          (t
           (values (insert-at-cnadr at after-apply formula)
                   theorem at formula
                   )))))


;;;; search

(defun apply-all-cases-once (steps-formula theorems)
  ;; (apply-all-cases-once '(* 4 (+ 3 3)) *first-axioms*) ;; example execute
  (let* ((cnadr-list (cnadr-list steps-formula))
         (all-applies (cartesian cnadr-list theorems)))
    all-applies
    (remove +failure+
            (apply #'append
                   (mapcar
                    #'(lambda (theorem)
                        (mapcar #'(lambda (adx)
                                    (multiple-value-bind (after theorem at before)
                                        (maybe-apply-theorem-at theorem steps-formula adx)
                                      (if (eq +failure+ after)
                                          +failure+
                                          (list after theorem at before))))
                                cnadr-list))
                    theorems)))))


(defun search-proof-aux (formula-objective theorems queue passed &key (n-search 10000) (show nil))
  (when show
    (format t "~%=======~%")
    (format t "~A~%" (car queue)))
  (let* ((step-subject (car queue))
         (step-formula (caar step-subject))
         (apply-onces
           (remove-if #'(lambda (into) ;; remove passed formulas.
                          (find (car into) passed :test #'equal))
                      (apply-all-cases-once step-formula theorems)))
         (notes-of-step (mapcar #'(lambda (app) (cons app step-subject)) apply-onces))
         (solvedp (find-if #'(lambda (note)
                               #|(format t "L: ~A, ~A, ~A~%"
                                       (caar note) formula-objective
                                       (equal formula-objective (caar note)))|#
                               (equal formula-objective (caar note)))
                           notes-of-step))
         (next-queue (append (cdr queue) notes-of-step)))
    (cond
      (solvedp
       (reverse solvedp))
      ((null queue)
       (values +failure+ passed))
      ((> n-search 1)
       (search-proof-aux formula-objective theorems next-queue
                         (append passed (mapcar #'car apply-onces))
                         :n-search (- n-search 1) :show show))
      (t
       (format t "failed to search search until limit.~%")
       (values +failure+ passed))
      )))





(defun search-proof (formula-objective formula-start theorems &key (n-search 10000) (show nil))
  (search-proof-aux formula-objective
                    theorems
                    `(((,formula-start () () ())))
                    `(,formula-start)
                    :n-search n-search
                    :show show
                    ))



;;;; memo




'("a*b = b*a"
  "a+b = b+a"
  "+b  = -(-b)")

;;;; type by proofing of in and out


;;;; rule bases :: axiom

;;;; knowledge base

;; first dictionaly of theorems

;; (defun add-knowledge (knowledge-base fact))


;;;; searching micro prolog implementation


;;;; ?- what is something-B of formula-A




;;;; def length of life

;;;; freeze and unfreeze, continuation

;;;; enviroment

;;;; interrupt

;;;; 



