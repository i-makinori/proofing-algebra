(in-package :proof-algebra)




;;; test for unification.
;; unification is function which matches unknows.

#|
;; for test
(apply-uni_test '*unicase-simple*) ;; to test specipic case
(apply-uni_test-all)               ;; to test all registerd cases
|#


(defvar *uni_test-cases*
  '())

(defun add-uni_test (name unknowns pattern1 pattern2 result &optional wrapping-bind)
  (setf *uni_test-cases*
        (cons (cons name
                    (cons (list unknowns pattern1 pattern2 wrapping-bind)
                          result))
              (remove-if #'(lambda (pair) (equal (car pair) name))
                         *uni_test-cases*))))

(defmacro def-uni_test (name unknowns pattern1 pattern2 result &optional wrapping-bind)
  `(add-uni_test ',name ',unknowns ',pattern1 ',pattern2 ',result ',wrapping-bind))


(defun apply-uni_test (unicase-name &optional (case-list *uni_test-cases*))
  (let ((current-case (find-if #'(lambda (case_n) (equal unicase-name (car case_n))) case-list)))
    (format t "===========~%")
    (cond
      (current-case
       (let* ((case-name-name (car current-case))
              (problem (cadr current-case))
              (answer (cddr current-case))
              (returns (unification (car problem) (cadr problem) (caddr problem) (cadddr problem)))
              (successp (set-equalp answer returns)))
         (format t "casename: ~A~%" case-name-name)
         (format t "args: ~A~%true-ansewer: ~A~%returns: ~A~%" problem answer returns)
         (format t "T/F: ~A~%" successp)
         successp))
      (t
       (lwarn "uni_test case named ~A is not found." unicase-name)
       +failure+))))



(defun apply-uni_test-all (&optional (case-names (mapcar #'car *uni_test-cases*)))
  (mapcar #'(lambda (name) (apply-uni_test name *uni_test-cases*)) case-names))


(defun refresh-uni_test-cases! ()
  (setf *uni_test-cases* nil)
  (progn 
    (def-uni_test *unicase-simple* (x) (x 1) (5 1) ((x . 5)))

    (def-uni_test *unicase-failure* (x y) (+ 2 3) (* 2 3) +failure+)
    (def-uni_test *unicase-success-nil* (x y) (+ 2 3) (+ 2 3) ()) ;; x and y are free
    
    (def-uni_test *unicase-both* (x y z) (* x z) (* y 4) ((x . y) (y . x) (z . 4)))
    
    (def-uni_test *unicase-duplicate-a* (x) (* x 5) (* 2 x) +failure+)
    (def-uni_test *unicase-duplicate-b* (x) (* x 3) (* 3 x) ((x . 3)))
    
    (def-uni_test *unicase-tree-a* (f x y) (+ (* c1 c2) y) (f (* c1 x) (* c3 c5))
      ((f . +) (x . c2) (y . (* c3 c5))))
    (def-uni_test *unicase-tree-b* (f x y) (f) ((+ x y)) ((f . (+ x y)))) ;; x and y are free
    
    (def-uni_test *unicase-unnproc1* ((++ x y) a) ((++ x y) 3) (b 3)
      (((++ x y) . b)))
    
    (def-uni_test *unicase-unnproc2* ((++ x y) a b) ((++ x y) a) (b (++ x y))
      (((++ x y) . b) (b . (++ X Y)) (a . (++ x y)) ((++ x y) . a)))
    ;; in this case, i = j = k form is needed to be implemented at another.
    ;;
    )
  
  (setf *uni_test-cases* (reverse *uni_test-cases*)))


(progn 
  (refresh-uni_test-cases!)
)

