(in-package :proof-algebra)


;;;; utilities of sets, binds, trees.

;;; set theory
(defun cartesian-1 (set-a set-b)
  (apply #'append
          (mapcar #'(lambda (a)
                      (mapcar #'(lambda (b) (cons a b)) set-b))
                  set-a)))

(defun cartesian (&rest sets)
  (reduce #'(lambda (a b)
              (cartesian-1 a b))
          (append sets '((())))
          :from-end t))


;;; binds

(defun apply-binds (unknowns form binds &optional form-after)
  ;; Q: (apply-binds '(a b) '(+ (a x x 3) b) '((a . z) (x . 2) (b . 4))) --> (+ (Z X X 3) 4) ;;? why?
  ;; A: unknown is '(a b) now. x is not registered.
  ;; (format t "~A, ~A, ~A~%" form binds form-after)
  (cond ((null form)
         (reverse form-after))
        ((unknownp form unknowns)
         (cdr (assoc form binds)))
        ((listp form)
         (apply-binds unknowns (cdr form) binds
                      (cons (apply-binds unknowns (car form) binds) form-after)))
        ((atom form)
         form)))

;;; tree positions

(defun cnadr-aux (list-cnadr tree)
  (cond ((null list-cnadr)
         tree)
        ((eq 'a (car list-cnadr))
         (cnadr-aux (cdr list-cnadr) (car tree)))
        ((eq 'd (car list-cnadr))
         (cnadr-aux (cdr list-cnadr) (cdr tree)))
        ((member (car list-cnadr) '(c r))
         (cnadr-aux (cdr list-cnadr) tree))
        (t
         (error (format nil "list-nad of cnadr need to be the list of a or d: ~A~%" list-cnadr)))))

(defun cnadr (list-cnadr tree)
  "address at the tree"
  (cnadr-aux (reverse list-cnadr) tree))

(defun cnadr-list (tree &optional cnadr_current in-listp)
  (cond ((null tree) '())
        ((atom tree) (list cnadr_current))
        (t
         (let ((each-of (append (cnadr-list (car tree) (cons 'a cnadr_current) nil)
                              (cnadr-list (cdr tree) (cons 'd cnadr_current) t))))
           (if in-listp
               each-of
               (cons cnadr_current each-of))))))


(defun insert-at-cnadr-aux (list-cnadr insert tree)
  (cond ((null list-cnadr)
         insert)
        ((eq 'a (car list-cnadr))
         (cons 
          (insert-at-cnadr-aux (cdr list-cnadr) insert (car tree))
          (cdr tree)))
        ((eq 'd (car list-cnadr))
         (cons
          (car tree)
          (insert-at-cnadr-aux (cdr list-cnadr) insert (cdr tree))))
        ((member (car list-cnadr) '(c r))
         (insert-at-cnadr-aux (cdr list-cnadr) insert tree))
        (t
         (error (format nil "list-nad of cnadr need to be the list of a or d: ~A~%" list-cnadr)))))  

(defun insert-at-cnadr (list-cnadr insert tree)
  (insert-at-cnadr-aux (reverse list-cnadr) insert tree))
