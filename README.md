# proofing-algebra

proofing program of algabra theories.

代数系諸定理証明プログラム.


### 操作の例、入出力の例


```lisp
$ cd proof-algebra/.
$ ros -- call_proof_factorization.lisp

To load "proof-algebra":
  Load 1 ASDF system:
    proof-algebra
; Loading "proof-algebra"

因数分解 探索プログラム (LENNMA_digress Ver.0.001) 
入力する式の例:
(x + y)
(a * b)
((a + b) * (i + j))
(a * (i + j) + b * (j + i))
など
また、 quit を入力することで、プログラムを終了します。
開始式を入力して下さい[1] >>
    ((* X I) + (* J Y) + (* I Y) + (* J X))
((* X I) + (* J Y) + (* I Y) + (* J X))
目標式を入力して下さい[1] >>
  (* (+ X Y) (+ I J))
(* (+ X Y) (+ I J))
開始式の詳しい中置記法表現には、((((X * I) + (J * Y)) + (I * Y)) + (J * X)) が在る。
目標式の詳しい中置記法表現には、((X + Y) * (I + J)) が在る。
  上式 の位置root部 に A+B=B+A を適用すると
  ((J * X) + (((X * I) + (J * Y)) + (I * Y))) となる。 
  上式 の位置(A D D)部 に A+B=B+A を適用すると
  ((J * X) + ((I * Y) + ((X * I) + (J * Y)))) となる。 
  上式 の位置(A D A D D A D D)部 に A*B=B*A を適用すると
  ((J * X) + ((I * Y) + ((I * X) + (J * Y)))) となる。 
  上式 の位置(A D D)部 に A+{B+C}={A+B}+C を適用すると
  ((J * X) + (((I * Y) + (I * X)) + (J * Y))) となる。 
  上式 の位置(A D D)部 に A+B=B+A を適用すると
  ((J * X) + ((J * Y) + ((I * Y) + (I * X)))) となる。 
  上式 の位置(A D D A D D)部 に A+B=B+A を適用すると
  ((J * X) + ((J * Y) + ((I * X) + (I * Y)))) となる。 
  上式 の位置root部 に A+{B+C}={A+B}+C を適用すると
  (((J * X) + (J * Y)) + ((I * X) + (I * Y))) となる。 
  上式 の位置root部 に A+B=B+A を適用すると
  (((I * X) + (I * Y)) + ((J * X) + (J * Y))) となる。 
  上式 の位置(A D)部 に {C*A}+{C*B}=C*{A+B} を適用すると
  ((I * (X + Y)) + ((J * X) + (J * Y))) となる。 
  上式 の位置(A D)部 に A*B=B*A を適用すると
  (((X + Y) * I) + ((J * X) + (J * Y))) となる。 
  上式 の位置(A D D)部 に {C*A}+{C*B}=C*{A+B} を適用すると
  (((X + Y) * I) + (J * (X + Y))) となる。 
  上式 の位置(A D D)部 に A*B=B*A を適用すると
  (((X + Y) * I) + ((X + Y) * J)) となる。 
  上式 の位置root部 に {C*A}+{C*B}=C*{A+B} を適用すると
  ((X + Y) * (I + J)) となる。 
かの如く、((* X I) + (* J Y) + (* I Y) + (* J X)) から (* (+ X Y) (+ I J)) への変換は可能。
開始式を入力して下さい[2] >>
  quit
QUIT
またね!.
$ 
```


<!--
### todo

[ ] apply theorem for only explict unknown symbols.
  - (theorems are applied to all symbols current.)
-->


### References

- [記号処理プログラミング - 岩波書店](https://www.iwanami.co.jp/book/b475660.html)

### Contact

If you have any questions, please ask me feel free.

- Makinori Ikegami  
  Email: <maau3p@gmail.com>  
  Personal Portal: [https://i-makinori.gitlab.io/](https://i-makinori.gitlab.io/)  

