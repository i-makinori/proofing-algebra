
(in-package :cl-user)
(defpackage proof-algebra-asd
  (:use :cl :asdf))
(in-package :proof-algebra-asd)


(asdf:defsystem proof-algebra
  :license nil
  :version "0.0.1"
  :description "proof-algebra"
  :author "i-makinori"
  :depends-on ()
  :components
  ((:module
    "src"
    :components
    ((:file "package")
     (:file "unification")
     (:file "set-theory")
     (:file "notation")
     (:file "polynomial")
     (:file "compress")
     (:file "test")
     ;;
     
))))

